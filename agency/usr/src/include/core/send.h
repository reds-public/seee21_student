/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - January 2018: Baptiste Delporte
 *
 */

#ifndef SEND_H
#define SEND_H

#include <virtshare/soo.h>

void try_to_send_ME(void);
void sig_initiate_migration(int sig);

#endif /* SEND_H */
