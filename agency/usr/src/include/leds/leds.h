/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - February 2018: Baptiste Delporte
 *
 */

#ifndef LEDS_H
#define LEDS_H

#include <stdint.h>

#include <virtshare/soo.h>

#define SOO_N_LEDS				6

#define SOO_LED_SYSFS_PREFIX			"/sys/class/leds/led_d"
#define SOO_LED_SYSFS_SUFFIX			"/brightness"

void led_set(uint32_t id, uint8_t value);
void led_on(uint32_t id);
void led_off(uint32_t id);

void leds_init(void);

#endif /* LEDS_H */
