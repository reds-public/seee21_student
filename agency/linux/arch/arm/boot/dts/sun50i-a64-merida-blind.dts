/*
 * Copyright (c) 2016 ARM Ltd.
 *
 * This file is dual-licensed: you can use it either under the terms
 * of the GPL or the X11 license, at your option. Note that this dual
 * licensing only applies to this file, and not this project as a
 * whole.
 *
 *  a) This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation; either version 2 of the
 *     License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 * Or, alternatively,
 *
 *  b) Permission is hereby granted, free of charge, to any person
 *     obtaining a copy of this software and associated documentation
 *     files (the "Software"), to deal in the Software without
 *     restriction, including without limitation the rights to use,
 *     copy, modify, merge, publish, distribute, sublicense, and/or
 *     sell copies of the Software, and to permit persons to whom the
 *     Software is furnished to do so, subject to the following
 *     conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *     OTHER DEALINGS IN THE SOFTWARE.
 */

/dts-v1/;

#include "sun50i-a64.dtsi"

#include <dt-bindings/gpio/gpio.h>

/ {
	model = "MERIDAv2";
	compatible = "meridav2,meridav2", "allwinner,sun50i-a64";

	aliases {
		ethernet0 = &emac;
		serial0 = &uart0;
		serial1 = &uart1;
		serial2 = &uart2;
		serial3 = &uart3;
		serial4 = &uart4;
	};

	chosen {
	    bootargs = "earlyprintk=sunxi-uart,0x01c28000 loglevel=8 console=ttyS0,115200n8 init=/init";
		stdout-path = "serial0:115200n8";
		linux,initrd-start = <0x0 0x0>;
		linux,initrd-end = <0x0 0x0>;
	};

	agency {
		domain-size = <0x20000000>; /* 512 MB */
	
         /* Backends */
	 backends {
		
            vuart {
        	compatible = "vuart,backend";
                status = "ok";
            };
						            			
            vuihandler {
		compatible = "vuihandler,backend";
		status = "ok";
            };
            
            vdoga12v6nm {
        	compatible = "vdoga12v6nm,backend";
                status = "ok";
            };
				
	 };
	
    };

    psci {
   		compatible	=  "arm,psci-0.2";
   		method		=  "smc";
   		psci_version =        <0x84000000>;
   		cpu_suspend	=         <0xc4000001>;
   		cpu_off		=         <0x84000002>;
   		cpu_on		=         <0xc4000003>;
   		affinity_info =       <0xc4000004>;
   		migrate		=         <0xc4000005>;
   		migrate_info_type =   <0x84000006>;
   		migrate_info_up_cpu = <0xc4000007>;
   		system_off =          <0x84000008>;
   		system_reset =        <0x84000009>;

	};
	
	firmware {
		optee {
			compatible = "linaro,optee-tz";
			method = "smc";
		};
	};

};
&pio {

	drv8703q1_pins: drv8703q1@0 {
		/* allwinner,pins = "PC0"; -- PC0 for the v1 */
		allwinner,pins = "PD23", "PD24"; /* PD23 nSLEEP, PD24 MODE */
		allwinner,function = "gpio_out";
		bias-disable;
		drive-strength = <10>;
	};

	avdd_pins: avdd@0 {
		allwinner,pins = "PH11";
		allwinner,function = "gpio_out";
		bias-disable;
		drive-strength = <10>;
	};
	
	down_pins_a: down_pins@0 {
		allwinner,pins = "PD20";
		allwinner,function = "gpio_in";
		drive-strength = <0>;
		bias-disable;
	};
	
};

&r_pio {
	up_pins_a: up_pins@0 {
		allwinner,pins = "PL3";
		allwinner,function = "gpio_in";
		allwinner,drive = <0>;
		allwinner,pull = <1>;
	};
};

&soc {
	blind_gpios: blind_gpios {
		pinctrl-0 = /*<&up_pins_a>,*/ <&down_pins_a>;
		status = "okay";
	};
};

&r_rsb {
	status = "okay";

	axp803: pmic@3a3 {
		compatible = "x-powers,axp803";
		reg = <0x3a3>;
		interrupt-parent = <&r_intc>;
		interrupts = <0 IRQ_TYPE_LEVEL_LOW>;
	};
};


&spi1 {
	status = "okay";

	drv8703q1@0 {
		//pinctrl-names = "sleep_gpio", "mode-gpio";
		//pinctrl-0 = <&drv8703q1_pins>;

		compatible = "ti,drv8703q1";
		reg = <0>;
		spi-max-frequency = <100000>;
		//sleep-gpio = <&pio 2 0 GPIO_ACTIVE_HIGH>; /* PC0 for v1*/
		sleep-gpio = <&pio 3 23 GPIO_ACTIVE_HIGH>; /* PD23 for v2*/
		mode-gpio = <&pio 3 24 GPIO_ACTIVE_HIGH>; /* PD23 for v2*/
	};
};

&i2c0 {
    status = "okay";
    pinctrl-names = "default";
    pinctrl-0 = <&i2c0_pins>;

    ds1050@28 {
        compatible = "maxim,ds1050";
        reg = <0x28>; /* This is due to the control byte (101) + A2, A1, A0 (000) combination (101000 = 0x28) */
    };
};


#include "axp803.dtsi"

&uart0 {
    pinctrl-names = "default", "sleep";
	pinctrl-0 = <&uart0_pins_a>;	 
	uart0_port = <0>;
	uart0_type = <4>;
	status = "okay";
};

 
&mmc1 {
	pinctrl-names = "default";
	pinctrl-0 = <&mmc1_pins>;
	vmmc-supply = <&reg_dcdc1>;
    vqmmc-supply = <&reg_eldo1>; 
	bus-width = <4>;
	non-removable;
	status = "okay";
	sd-uhs-sdr104;
	mwifiex {
		compatible = "marvell,sd8897";
		reg = <1>;
	};
	
	btmrvl: btmrvl@2 {
		compatible = "marvell,sd8897-bt";
		reg = <2>;
	};
};

&mmc2 {
	pinctrl-names = "default";
	pinctrl-0 = <&mmc2_pins>;
	vmmc-supply = <&reg_dcdc1>;
	vqmmc-supply = <&reg_eldo1>;
	bus-width = <8>;
	non-removable;
	cap-mmc-hw-reset;
	status = "okay";
	mmc-hs200-1_8v;
};

&reg_dcdc1 {
	regulator-always-on;
	regulator-min-microvolt = <3300000>;
	regulator-max-microvolt = <3300000>;
	regulator-name = "vcc-3v3";
	status = "okay";
};

&reg_eldo1 {
	regulator-always-on;
	regulator-min-microvolt = <1800000>;
	regulator-max-microvolt = <1800000>;
	regulator-name = "cpvdd";
	status = "okay";
};

#if 0 /* SOO.tech */ /* Not used at the moment */
&reg_aldo2 {
	regulator-always-on;
	regulator-min-microvolt = <1800000>;
	regulator-max-microvolt = <3300000>;
	regulator-name = "vcc-pl";
};

&reg_aldo3 {
	regulator-always-on;
	regulator-min-microvolt = <3000000>;
	regulator-max-microvolt = <3000000>;
	regulator-name = "vcc-pll-avcc";
};

&reg_dc1sw {
	regulator-name = "vcc-phy";
};

#endif /* 0 */

&reg_dcdc2 {
	regulator-always-on;
	regulator-min-microvolt = <1040000>;
	regulator-max-microvolt = <1300000>;
	regulator-name = "vdd-cpux";
	status = "okay";
};

/* DCDC3 is polyphased with DCDC2 */

/*
 * The DRAM chips used by Pine64 boards are DDR3L-compatible, so they can
 * work at 1.35V with less power consumption.
 * As AXP803 DCDC5 cannot reach 1.35V accurately, use 1.36V instead.
 */
&reg_dcdc5 {
	regulator-always-on;
	regulator-min-microvolt = <1360000>;
	regulator-max-microvolt = <1360000>;
	regulator-name = "vcc-dram";
	status = "okay";
};

#if 0 /* SOO.tech */
&reg_dcdc6 {
	regulator-always-on;
	regulator-min-microvolt = <1100000>;
	regulator-max-microvolt = <1100000>;
	regulator-name = "vdd-sys";
};

&reg_dldo1 {
	regulator-min-microvolt = <3300000>;
	regulator-max-microvolt = <3300000>;
	regulator-name = "vcc-hdmi";
};

&reg_dldo2 {
	regulator-min-microvolt = <3300000>;
	regulator-max-microvolt = <3300000>;
	regulator-name = "vcc-mipi";
};

&reg_dldo4 {
	regulator-min-microvolt = <3300000>;
	regulator-max-microvolt = <3300000>;
	regulator-name = "vcc-wifi";
};

&reg_fldo1 {
	regulator-min-microvolt = <1200000>;
	regulator-max-microvolt = <1200000>;
	regulator-name = "vcc-1v2-hsic";
};
#endif /* 0 */

/*
 * The A64 chip cannot work without this regulator off, although
 * it seems to be only driving the AR100 core.
 * Maybe we don't still know well about CPUs domain.
 */
&reg_fldo2 {
	regulator-always-on;
	regulator-min-microvolt = <1100000>;
	regulator-max-microvolt = <1100000>;
	regulator-name = "vdd-cpus";
};

#if 0 /* SOO.tech */

&reg_rtc_ldo {
	regulator-name = "vcc-rtc";
};

&simplefb_hdmi {
	vcc-hdmi-supply = <&reg_dldo1>;
};
#endif /* 0 */

#if 0 /* SOO.tech */

/* On Pi-2 connector */
&uart2 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart2_pins>;
	status = "disabled";
};

/* On Euler connector */
&uart3 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart3_pins>;
	status = "disabled";
};

/* On Euler connector, RTS/CTS optional */
&uart4 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart4_pins>;
	status = "disabled";
};

&usb_otg {
	dr_mode = "host";
	status = "okay";
};

&usbphy {
	status = "okay";
};

#endif /* 0 */

 
	
