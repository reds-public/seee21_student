#include <vfs.h>

#include <device/driver.h>

/* to be completed */

struct file_operations mydev_fops = {
	/* to be completed */
};

struct devclass mydev_dev = {
	.class = "mydev",
	.type = VFS_TYPE_DEV_CHAR,
	.fops = &mydev_fops,
};


int mydev_init(dev_t *dev) {

	/* Register the framebuffer so it can be accessed from user space. */
	devclass_register(dev, &mydev_dev);

	return 0;
}


REGISTER_DRIVER_POSTCORE("arm,mydev", mydev_init);
