/*
 * Copyright (C) 2018-2019 Daniel Rossier <daniel.rossier@soo.tech>
 * Copyright (C) 2018-2019 Baptiste Delporte <bonel@bonel.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#if 0
#define DEBUG
#endif

#include <heap.h>
#include <mutex.h>
#include <completion.h>
#include <memory.h>

#include <uapi/linux/input.h>

#include <device/device.h>
#include <device/driver.h>

#include <soo/evtchn.h>
#include <soo/vbus.h>
#include <soo/console.h>
#include <soo/debug.h>
#include <soo/gnttab.h>

#include <soo/dev/vvext.h>

/* Our unique vvext instance. */
static struct vbus_device *__vvext = NULL;

irq_return_t vvext_interrupt(int irq, void *dev_id) {

	/* to be completed */

	return IRQ_COMPLETED;
}

void vvext_probe(struct vbus_device *vdev) {
	int res;
	unsigned int evtchn;
	vvext_sring_t *sring;
	struct vbus_transaction vbt;
	vvext_t *vvext;

	DBG0("[vvext] Frontend probe\n");

	if (vdev->state == VbusStateConnected)
		return ;

	vvext = malloc(sizeof(vvext_t));
	BUG_ON(!vvext);
	memset(vvext, 0, sizeof(vvext_t));

	/* Local instance */
	__vvext = vdev;

	dev_set_drvdata(vdev->dev, &vvext->vdevfront);

	DBG("Frontend: Setup ring\n");

	/* Prepare to set up the ring. */

	vvext->ring_ref = GRANT_INVALID_REF;

	/* Allocate an event channel associated to the ring */
	res = vbus_alloc_evtchn(vdev, &evtchn);
	BUG_ON(res);

	res = bind_evtchn_to_irq_handler(evtchn, vvext_interrupt, NULL, vdev);
	if (res <= 0) {
		lprintk("%s - line %d: Binding event channel failed for device %s\n", __func__, __LINE__, vdev->nodename);
		BUG();
	}

	vvext->evtchn = evtchn;
	vvext->irq = res;

	/* Allocate a shared page for the ring */
	sring = (vvext_sring_t *) get_free_vpage();
	if (!sring) {
		lprintk("%s - line %d: Allocating shared ring failed for device %s\n", __func__, __LINE__, vdev->nodename);
		BUG();
	}

	SHARED_RING_INIT(sring);
	FRONT_RING_INIT(&vvext->ring, sring, PAGE_SIZE);

	/* Prepare the shared to page to be visible on the other end */

	res = vbus_grant_ring(vdev, phys_to_pfn(virt_to_phys_pt((uint32_t) vvext->ring.sring)));
	if (res < 0)
		BUG();

	vvext->ring_ref = res;

	vbus_transaction_start(&vbt);

	vbus_printf(vbt, vdev->nodename, "ring-ref", "%u", vvext->ring_ref);
	vbus_printf(vbt, vdev->nodename, "ring-evtchn", "%u", vvext->evtchn);

	vbus_transaction_end(vbt);

}

void vvext_shutdown(struct vbus_device *vdev) {

	DBG0("[vvext] Frontend shutdown\n");
}

void vvext_closed(struct vbus_device *vdev) {
	vvext_t *vvext = to_vvext(vdev);

	DBG0("[vvext] Frontend close\n");

	/**
	 * Free the ring and deallocate the proper data.
	 */

	/* Free resources associated with old device channel. */
	if (vvext->ring_ref != GRANT_INVALID_REF) {
		gnttab_end_foreign_access(vvext->ring_ref);
		free_vpage((uint32_t) vvext->ring.sring);

		vvext->ring_ref = GRANT_INVALID_REF;
		vvext->ring.sring = NULL;
	}

	if (vvext->irq)
		unbind_from_irqhandler(vvext->irq);

	vvext->irq = 0;
}

void vvext_connected(struct vbus_device *vdev) {

	DBG0("[vvext] Frontend connected\n");

}

void vvext_suspend(struct vbus_device *vdev) {

	DBG0("[vvext] Frontend suspend\n");
}

void vvext_resume(struct vbus_device *vdev) {

	DBG0("[vvext] Frontend resume\n");
}

vdrvfront_t vvext_drv = {
	.probe = vvext_probe,
	.shutdown = vvext_shutdown,
	.closed = vvext_closed,
	.suspend = vvext_suspend,
	.resume = vvext_resume,
	.connected = vvext_connected
};

static int vvext_init(dev_t *dev) {

	vdevfront_init(VVEXT_NAME, &vvext_drv);

	return 0;
}

REGISTER_DRIVER_POSTCORE("vvext,frontend", vvext_init);
