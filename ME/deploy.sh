#!/bin/bash

echo Deploying all MEs into the third partition...


cd ../agency/filesystem

./mount.sh 3 vexpress

sudo rm -rf fs/*

if [ "$1" != "clean" ]; then
        
        
        cd ../../ME/usr
        ./deploy.sh so3virt

        cd ../target
        ./mkuboot.sh meso3

        cd ../../agency/filesystem
        ME_to_deploy="../../ME/target/meso3.itb"
        sudo cp -rf $ME_to_deploy fs/so3virt.itb
        echo "$1 deployed"
fi
./umount.sh
