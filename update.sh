#!/bin/bash

echo Updating Eclipse metadata.

rm pack.tar.gz
rm -rf .metadata
find . -name ".project" | xargs rm -rf
find . -name ".cproject" | xargs rm -rf
find . -name ".settings" | xargs rm -rf

wget http://reds-data.heig-vd.ch/cours/seee/pack.tar.gz &> /dev/null

if [ ! $? -eq 0 ]; then
	echo "Error retriving metadata. Be sure to be connected to the VPN."
	exit -1
fi

echo Extracting metadata...
tar xf pack.tar.gz
